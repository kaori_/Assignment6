public class Food extends Product{
    private int foodCalorie;
    private int foodSize;
    private String[] foodIngredients;

    Food(int id, String name, float price, String country, int calorie, int size, String[] ingredients) {
        super(id, name, price, country);
        this.foodCalorie = calorie;
        this.foodSize = size;
        this.foodIngredients = ingredients;
    }

}
