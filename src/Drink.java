public class Drink extends Product {
    private boolean isDrinkDiet;
    private int drinkSize;

    Drink(int id, String name, float price, String country, boolean isDrinkDiet, int size) {
        super(id, name, price, country);
        this.isDrinkDiet = isDrinkDiet;
        this.drinkSize = size;
    }
}
