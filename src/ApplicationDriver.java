import java.util.ArrayList;

public class ApplicationDriver {

    public static void main(String[] args) {
        ArrayList<Product> products;
        products = createProducts();

        ShoppingCart myCart = new ShoppingCart();
        myCart.add(new OrderProduct(products.get(0), 3));
        myCart.add(new OrderProduct(products.get(1), 1));
        myCart.add(new OrderProduct(products.get(2), 2));
        myCart.add(new OrderProduct(products.get(3), 2));
        myCart.add(new OrderProduct(products.get(4), 1));

        myCart.printProducts();

        System.out.println("================");
        System.out.print("Total:");
        System.out.println("$ " +myCart.getTotalAmount());

    }

    private static ArrayList<Product> createProducts() {
        String[] ingredientsChicken = {"chicken", "oil", "cheese"};
        String[] ingredientsBeef = {"beef", "oil", "basil"};
        String[] ingredientsPasta = {"pasta", "meet", "spinach"};

        ArrayList<Material> materials = new ArrayList<>();
        materials.add(new Material(10, "Cotton"));
        materials.add(new Material(11, "Nylon"));
        materials.add(new Material(12, "Polyester"));

        Material[] materialsDress = {materials.get(0), materials.get(2)};
        Material[] materialsTshirt = {materials.get(0), materials.get(2)};


        ArrayList<Product> products = new ArrayList<>();

        products.add(new Drink(412, "Pepsi", 2.0f, "USA", false, 150));
        products.add(new Drink(183, "Ginger Zero", 3.0f, "Canada", true, 200));
        products.add(new Food(100, "Chicken", 8.0f, "Canada", 350, 4, ingredientsChicken));
        products.add(new Food(101, "Pasta", 18.0f, "Canada", 250, 3, ingredientsPasta));
        products.add(new Cloth(701, "T-shirt", 15.00f, "China", materialsTshirt));


        products.add(new Drink(110, "Diet Pepsi", 2.0f, "USA", false, 150));
        products.add(new Food(105, "Beef", 12.0f, "Canada", 395, 2, ingredientsBeef));
        products.add(new Cloth(702, "Dress", 110.98f, "Japan", materialsDress));

        return products;
    }
}
