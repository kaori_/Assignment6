public class Product {
    private int productID;
    private String productName;
    private float productPrice;
    private String productMadeInCountry;

    Product(int id, String name, float price, String country) {
        this.productID = id;
        this.productName = name;
        this.productPrice = price;
        this.productMadeInCountry = country;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public String getProductName() {
        return productName;
    }
}
