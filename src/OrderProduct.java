public class OrderProduct {
    private Product product;
    private int qty;

    OrderProduct(Product product, int qty) {
        this.product = product;
        this.qty = qty;
    }

    public Product getProduct() {
        return product;
    }

    public float getAmount() {
        return this.product.getProductPrice()*this.qty;
    }
}
