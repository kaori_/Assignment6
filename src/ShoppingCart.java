import java.util.ArrayList;

public class ShoppingCart {
    private ArrayList<OrderProduct> cart;

    ShoppingCart () {
        cart = new ArrayList<>();
    }
    public void add (OrderProduct product) {
        cart.add(product);
    }

    public float getTotalAmount() {
        float totalAmount = 0;
        for(OrderProduct op : cart) {
            totalAmount += op.getAmount();
        }

        return totalAmount;
    }

    public void printProducts() {
        for(OrderProduct op: cart) {
            System.out.println(op.getProduct().getProductName());
        }
    }
}
