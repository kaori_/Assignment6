public class Cloth extends Product{
    private Material[] ClothMaterials;

    Cloth(int id, String name, float price, String country, Material[] materials) {
        super(id, name, price, country);
        this.ClothMaterials = materials;
    }

}
